﻿
using System;
using PnId_Instruments.DependencyInjection;
using PnId_Instruments.ManageFiles.Contracts;
using PnId_Instruments.ManageFiles.Services.Excel;
using Xunit;


namespace PnID_Instruments_Tests.Integration.Excel
{
    public class ExcelManagerTests : IDisposable
    {
        private IExcelManager _excelManager;
        private string _testFileNameAndPath =
            @"C:\Users\fcviw\Development\projects\pnid\test_docs\testPnid.xlsx";
        private string _testSheetName = "equipment";


        // Initialize attributes
        public ExcelManagerTests()
        {
            // Set up Depxhendency Injection
            DependencyInjectionSetup.Initialize();
            _excelManager = DependencyInjectionSetup.GetService<IExcelManager>();
            _excelManager.InitializeExcelProcess(_testFileNameAndPath, FileExcelMode.CreateNew);
            _excelManager.AddNewSheet(_testSheetName);
        }

        [Fact]
        public void WriteAndReadCellTest()
        {
            // Given
            string expedtedValue = "Hello, Excel DI";
            int testRow = 1, testCol = 2;

            // When
            _excelManager
                .WriteToCell(testRow, testCol, expedtedValue, _testSheetName);
            string actualValue =
                _excelManager.ReadFromCell(testRow, testCol, _testSheetName);

            // Assert
            Assert.Equal(expedtedValue, actualValue);
        }

        // Cleanup after tests
        public void Dispose()
        {
            _excelManager.SaveAs(_testFileNameAndPath);
        }
    }
}
